const endpoint = process.env.DB_ENDPOINT || 'http://localhost:8000';
const region = process.env.REGION || 'us-west-2';
const accessKeyId = process.env.AWS_ACCESS_KEY_ID || 'DEVAPIKey';
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY || 'DEVSecretAPIKey';

exports.localConfig = { region, endpoint, accessKeyId, secretAccessKey };