const express = require('express');
const router = express.Router();
const userController = require('./controllers/user');
const authController = require('./controllers/auth');
const boardsController = require('./controllers/boards');
const discussionsController = require('./controllers/discussions');
const topicsController = require('./controllers/topics');


router.route('/')
  .get((req, res) => res.json({ status: 'ok' }));

router.route('/register')
  .post(userController.addUser);

router.route('/login')
  .get(authController.isAuthenticated, userController.getUser);

router.route('/boards')
  .get(authController.hasValidToken, boardsController.getBoards)
  .post(authController.hasValidToken, boardsController.postBoard);

router.route('/boards/:boardId')
  .get(boardsController.getBoard)
  .delete(authController.hasValidToken, boardsController.deleteBoard);

router.route('/boards/:boardId/discussions')
  .get(discussionsController.getDiscussions)
  .post(authController.hasValidToken, discussionsController.postDiscussion);

router.route('/boards/:boardId/discussions/:discussionId')
  .put(authController.hasValidToken, discussionsController.updateDiscussion)
  .delete(authController.hasValidToken, discussionsController.deleteDiscussion);

router.route('/discussions/:discussionId/topics')
  .get(topicsController.getTopics)
  .post(topicsController.postTopic);

router.route('/discussions/:discussionId/topics/:topicId')
  .delete(authController.hasValidToken, topicsController.deleteTopic);

router.route('/topics/:topicId/votes')
  .post(topicsController.postVote);

module.exports = router;