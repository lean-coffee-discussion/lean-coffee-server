const Discussion = require('../../models/discussion');
const Topic = require('../../models/topic');

exports.getTopics = (req, res) => {
  Discussion.findOne(req.params.discussionId, (err, discussion) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!discussion) {
      return res.status(404).send();
    }
    if (!discussion.topics || discussion.topics.length === 0) {
      return res.json({ topics: [] });
    }
    Topic.find(discussion.topics, (err, topics) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      if (!topics) {
        return res.json({ topics: [] });
      }
      return res.json({ topics: topics });
    });
  });
}

exports.postTopic = (req, res) => {
  Discussion.findOne(req.params.discussionId, (err, discussion) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!discussion) {
      return res.status(404).send();
    }
    discussion.addTopic(req.body.title, req.body.index, (err, topic) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      return res.status(201).json(topic);
    });
  });
}

exports.deleteTopic = (req, res) => {
  if (!req.user || !req.user.username) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  Discussion.findOne(req.params.discussionId, (err, discussion) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!discussion) {
      return res.status(404).send();
    }
    discussion.deleteTopic(req.params.topicId, (err, success) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      if (!success) {
        return res.status(400).send({ message: 'Error deleting topic!' });
      }
      return res.status(204).send();
    });
  });
}

exports.postVote = (req, res) => {
  if (!req.params.topicId) {
    return res.status(400).send({ message: 'No Topic ID specified' });
  }
  Topic.findOne(req.params.topicId, (err, topic) => {
    if (err) {
      return res.status(400).send({ message: 'Error finding topic!' });
    }
    if (!topic) {
      return res.status(404).send({ message: 'Topic not found!' });
    }
    topic.addVote((err, topic) => {
      if (err) {
        return res.status(400).send({ message: 'Error adding vote!' });
      }
      if (!topic) {
        return res.status(400).send({ message: 'Could not add vote!' });
      }
      return res.status(200).json(topic);
    });
  });
}