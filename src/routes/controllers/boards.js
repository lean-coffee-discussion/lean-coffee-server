const Board = require('../../models/board');
const Discussion = require('../../models/discussion');
const Topic = require('../../models/topic');
const _ = require('lodash');

exports.getBoards = (req, res) => {
  if (!req.user) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  if (!req.user.boards) {
    return res.json({ boards: [] });
  }
  Board.find(req.user.boards, (err, boards) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!boards) {
      return res.json({ boards: [] });
    }
    return res.json({ boards: boards });
  });
};

exports.getBoard = (req, res) => {
  let normalizedResponse = {};
  if (!req.params.boardId) {
    return res.status(404).send();
  }
  normalizedResponse.result = req.params.boardId;
  normalizedResponse.entities = {};
  Board.findOne(req.params.boardId, (err, board) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!board) {
      return res.status(404).send();
    }
    normalizedResponse.entities.boards = { [req.params.boardId]: { ...board } };
    if (!board.discussions || board.discussions.length === 0) {
      return res.json(normalizedResponse);
    }
    Discussion.find(board.discussions, (err, discussions) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      if (!discussions || discussions.length === 0) {
        return res.json(normalizedResponse);
      }
      normalizedResponse.entities.discussions = _.keyBy(discussions, (discussion) => discussion.id);
      const topicArray = _.reduce(discussions, (array, discussion) => {
        if (!discussion.topics || discussion.topics.length === 0) {
          return array;
        }
        return _.concat(array, discussion.topics)
      }, []);
      if (!topicArray || topicArray.length === 0) {
        return res.json(normalizedResponse);
      }
      Topic.find(topicArray, (err, topics) => {
        if (err) {
          return res.status(err.statusCode).send({ message: err.message });
        }
        if (!topics || topics.length === 0) {
          return res.json(normalizedResponse);
        }
        normalizedResponse.entities.topics = _.keyBy(topics, (topic) => topic.id);
        return res.json(normalizedResponse);
      });
    });
  });
};

exports.postBoard = (req, res) => {
  if (!req.user || !req.user.username) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  req.user.addBoard(req.body.title, req.body.index, (err, board) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    return res.status(201).json(board);
  });
}

exports.deleteBoard = (req, res) => {
  if (!req.user || !req.user.username) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  req.user.deleteBoard(req.params.boardId, (err, success) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!success) {
      return res.status(400).send({ message: 'Error deleting board!' });
    }
    return res.status(204).send();
  });
}