const Board = require('../../models/board');
const Discussion = require('../../models/discussion');

exports.getDiscussions = (req, res) => {
  Board.findOne(req.params.boardId, (err, board) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!board) {
      return res.status(404).send();
    }
    if (!board.discussions || board.discussions.length === 0) {
      return res.json({ discussions: [] });
    }
    Discussion.find(board.discussions, (err, discussions) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      if (!discussions) {
        return res.json({ discussions: [] });
      }
      return res.json({ discussions: discussions });
    });
  });
}

exports.postDiscussion = (req, res) => {
  if (!req.user || !req.user.username) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  if (!req.user.boards || !req.user.boards.includes(req.params.boardId)) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  Board.findOne(req.params.boardId, (err, board) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!board) {
      return res.status(404).send();
    }
    board.addDiscussion(req.body.title, req.body.index, (err, discussion) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      return res.status(201).json(discussion);
    });
  });
}

exports.deleteDiscussion = (req, res) => {
  if (!req.user || !req.user.username) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  Board.findOne(req.params.boardId, (err, board) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!board) {
      return res.status(404).send();
    }
    board.deleteDiscussion(req.params.discussionId, (err, success) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      if (!success) {
        return res.status(400).send({ message: 'Error deleting discussion!' });
      }
      return res.status(204).send();
    });
  });
}

exports.updateDiscussion = (req, res) => {
  if (!req.user || !req.user.username) {
    return res.status(401).send({ message: 'Not Authorized!' });
  }
  Board.findOne(req.params.boardId, (err, board) => {
    if (err) {
      return res.status(err.statusCode).send({ message: err.message });
    }
    if (!board) {
      return res.status(404).send();
    }
    board.updateDiscussion({
      id: req.params.discussionId,
      title: req.body.title,
      index: req.body.index,
      topics: req.body.topics
    }, (err, discussion) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      if (!discussion) {
        return res.status(400).send({ message: 'Error updating discussion!' });
      }
      return res.json(discussion);
    });
  });
}