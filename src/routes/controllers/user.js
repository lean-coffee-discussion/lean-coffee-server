const jwt = require('jsonwebtoken');
const owasp = require('owasp-password-strength-test');
const jwtSecret = process.env.JWT_SECRET;
const jwtExpiration = '1d';
const User = require('../../models/user');

owasp.config({
  allowPassphrases: true,
  maxLength: 128,
  minLength: 8,
  minPhraseLength: 20,
  minOptionalTestsToPass: 3,
});

function generateToken(username) {
  const profile = {
    email: username
  };
  return jwt.sign(profile, jwtSecret, { expiresIn: jwtExpiration });
}

exports.getUser = (req, res) => {
  if (req.user && req.user.username) {
    res.json({ token: generateToken(req.user.username) });
  } else {
    res.status(401).send({ message: 'Not Authorized!' });
  }
}

exports.addUser = (req, res) => {
  if (!req.body.email || !req.body.password || !req.body.confirm) {
    return res.status(400).send({ message: 'User email, password and password confirmation are required!' });
  }
  const username = req.body.email.toLowerCase();
  const password = req.body.password;
  const userQuery = { username };
  User.findOne(userQuery, (err, userExists) => {
    if (userExists) {
      return res.status(409).send({ message: 'User already exists!' });
    }
    if (req.body.password !== req.body.confirm) {
      return res.status(400).send({ message: 'Password does not match!' });
    }
    const complexityTest = owasp.test(password);
    if (complexityTest.errors && complexityTest.errors.length > 0) {
      return res.status(400).send({ message: complexityTest.errors[0] });
    }
    User.insertOne({ username, password }, (err, user) => {
      if (err) {
        return res.status(err.statusCode).send({ message: err.message });
      }
      if (!user || !user.username) {
        res.status(400).send({ message: 'User could not be registered!' });
      }
      res.json({ token: generateToken(user.username) });
    });
  });
}