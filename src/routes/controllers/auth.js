const passport = require('passport');
const BasicStrategy = require('passport-http').BasicStrategy;
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../../models/user');

const secret = process.env.JWT_SECRET;

passport.use(new BasicStrategy(
  (username, password, done) => {
    User.findOne({ username: username }, (err, user) => {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      user.validatePassword(password, (error, isValid) => {
        if (error) {
          return done(error);
        }
        if (!isValid) {
          return done(null, false);
        }
        return done(null, { username: user.username });
      });
    });
  })
);

var opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = secret;
passport.use(new JwtStrategy(opts,
  (jwt_payload, done) => {
    User.findOne({ username: jwt_payload.email }, (err, user) => {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user);
    })
  })
);

exports.isAuthenticated = passport.authenticate('basic', { session: false });
exports.hasValidToken = passport.authenticate('jwt', { session: false });