'use strict'
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
const passport = require('passport');
const cors = require('cors');
const awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');

const routes = require('./routes/index');

app.use(cors({ credentials: true, origin: true }));

if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('tiny'));
}

if (process.env.NODE_ENV == 'production') {
  app.use(awsServerlessExpressMiddleware.eventContext());
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ type: 'application/json' }));

app.use(passport.initialize());

app.use('/api/v1', routes);

module.exports = app;
