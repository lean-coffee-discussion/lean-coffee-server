const app = require('./server');
const port = process.env.HOST_PORT || 8080;

const server = app.listen(port);
console.log(`listening on http://localhost:${port}`);

function stop() {
  server.close();
}

module.exports = app;
module.exports.stop = stop;