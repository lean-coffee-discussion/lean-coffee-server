const AWS = require('aws-sdk');
const AWSConfig = require('../config/dynamodb_local');
const cuid = require('cuid');
const Topic = require('./topic');

if (process.env.NODE_ENV === 'production') {
  AWS.config.update({ region: process.env.REGION });
} else {
  AWS.config.update(AWSConfig.localConfig);
}

const docClient = new AWS.DynamoDB.DocumentClient();
const tableName = process.env.DB_TABLE_DISCUSSIONS || 'Discussions';

function Discussion({ id = '', title = '', index = 0, topics }) {
  this.id = id;
  this.title = title;
  this.index = index;
  if (topics && topics.values) {
    this.topics = topics.values;
  }
  this.addTopic = addTopic;
  this.deleteTopic = deleteTopic;
  this.appendTopic = appendTopic;
  this.removeTopic = removeTopic;
  this.update = update;
}

function addTopic(title, index, done) {
  if (!title) {
    return done({ statusCode: 400, message: 'No title specified!' });
  }
  Topic.insertOne(title, index, (err, topic) => {
    if (err) {
      return done(err);
    }
    if (!topic || !topic.id) {
      return done(null, false);
    }
    this.appendTopic(topic.id, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, topic);
    });
  });
}

function deleteTopic(topicId, done) {
  if (!topicId) {
    return done({ statusCode: 400, message: 'No topic ID specified!' });
  }
  if (!this.topics || !this.topics.includes(topicId)) {
    return done(null, false);
  }
  Topic.deleteOne(topicId, (err, success) => {
    if (err) {
      return done(err);
    }
    if (!success) {
      return done({ statusCode: 400, message: 'Error deleting topic!' });
    }
    this.removeTopic(topicId, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, true);
    });
  });
}

function appendTopic(topicId, done) {
  if (!topicId) {
    return done({ statusCode: 400, message: 'No topic ID specified!' });
  }
  let updateExpr = '';
  if (!this.topics || this.topics.length === 0) {
    updateExpr = 'SET #attrName = :attrValue';
  } else {
    updateExpr = 'ADD #attrName :attrValue';
  }
  const params = {
    TableName: tableName,
    Key: {
      id: this.id
    },
    UpdateExpression: updateExpr,
    ExpressionAttributeNames: {
      "#attrName": "topics"
    },
    ExpressionAttributeValues: {
      ":attrValue": docClient.createSet([topicId])
    },
    ReturnValues: "UPDATED_NEW"
  };
  docClient.update(params, (err, data) => {
    if (err) {
      return done(err);
    }
    return done(null, true);
  });
}

function removeTopic(topicId, done) {
  if (!topicId) {
    return done({ statusCode: 400, message: 'No topic ID specified!' });
  }
  if (!this.topics || !this.topics.includes(topicId)) {
    return done(null, false);
  }
  const userParams = {
    TableName: tableName,
    Key: {
      id: this.id
    },
    UpdateExpression: "DELETE #attrName :attrValue",
    ExpressionAttributeNames: {
      "#attrName": "topics"
    },
    ExpressionAttributeValues: {
      ":attrValue": docClient.createSet([topicId])
    }
  }
  docClient.update(userParams, (err, data) => {
    if (err) {
      return done(err);
    }
    return done(null, true);
  });
}

function update({ id, title, index, topics }, done) {
  let updateTitleExpr = '';
  let updateTopicsExpr = '';
  let updateIndexExpr = '';
  let attrNames = {};
  let attrValues = {};
  if (topics) {
    updateTopicsExpr = '#topicName = :topicValue';
    attrNames['#topicName'] = 'topics';
    if (topics.length > 0) {
      attrValues[':topicValue'] = docClient.createSet(topics);
    } else if (topics.length === 0) {
      attrValues[':topicValue'] = null;
    }
  }
  if (title) {
    updateTitleExpr = '#titleName = :titleValue';
    attrNames['#titleName'] = 'title';
    attrValues[':titleValue'] = title;
  }
  if (index) {
    updateIndexExpr = '#indexName = :indexValue';
    attrNames['#indexName'] = 'index';
    attrValues[':indexValue'] = index;
  }
  if (Object.keys(attrNames).length === 0 || Object.keys(attrValues).length === 0) {
    return done(null, false);
  }
  let updateExpr = 'SET ';
  if (updateTitleExpr) {
    updateExpr += `${updateTitleExpr}, `;
  }
  if (updateIndexExpr) {
    updateExpr += `${updateIndexExpr}, `;
  }
  if (updateTopicsExpr) {
    updateExpr += `${updateTopicsExpr}, `;
  }
  if (updateExpr === 'SET ') {
    return done(null, false);
  }
  const trimmedExpr = updateExpr.replace(/,\s$/, '');
  const params = {
    TableName: tableName,
    Key: {
      id: this.id
    },
    UpdateExpression: trimmedExpr,
    ExpressionAttributeNames: attrNames,
    ExpressionAttributeValues: attrValues,
    ReturnValues: "ALL_NEW"
  };
  docClient.update(params, (err, data) => {
    if (err) {
      return done(err);
    }
    return done(null, data);
  });
}

exports.findOne = (id, done) => {
  if (!id) {
    return done({ statusCode: 400, message: 'No discussion ID specified!' });
  }
  const params = {
    TableName: tableName,
    Key: {
      id: id
    }
  }
  docClient.get(params, function (err, data) {
    if (err) {
      return done(err);
    }
    if (!data || !data.Item) {
      return done(null, false);
    }
    return done(null, new Discussion(data.Item));
  });
}

exports.find = (ids, done) => {
  if (!ids || ids.length === 0) {
    return done({ statusCode: 400, message: 'No discussion IDs specified!' });
  }
  const all = ids.map((id) => { return { id } });
  const params = {
    RequestItems: {
      [tableName]: {
        Keys: all
      }
    }
  }
  docClient.batchGet(params, (err, data) => {
    if (err) {
      return done(err);
    }
    if (!data || !data.Responses || !data.Responses[tableName] || data.Responses[tableName].length === 0) {
      return done(null, false);
    }
    return done(null, data.Responses[tableName].map((discussion) => new Discussion(discussion)));
  });
}

exports.insertOne = (title, index = 0, done) => {
  const params = {
    TableName: tableName,
    Item: {
      id: cuid(),
      title: title,
      index: index
    }
  };
  docClient.put(params, (err, data) => {
    if (err) {
      return done(err);
    }
    return done(null, new Discussion(params.Item));
  });
}

exports.deleteOne = (id, done) => {
  exports.findOne(id, (err, discussion) => {
    if (err) {
      return done(err);
    }
    if (discussion.topics && discussion.topics.length > 0) {
      discussion.topics.map((topic) => {
        discussion.deleteTopic(topic.id, (err, data) => {
          if (err) {
            return done(err);
          }
          if (!data) {
            return done(null, false);
          }
        });
      });
    }
    const params = {
      TableName: tableName,
      Key: {
        id: id
      }
    }
    docClient.delete(params, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, true);
    });
  });
}

exports.updateOne = ({ id, title, index, topics }, done) => {
  exports.findOne(id, (err, originalDiscussion) => {
    if (err) {
      return done(err);
    }
    originalDiscussion.update({ id, title, index, topics }, (err, data) => {
      if (err) {
        return done(err);
      }
      if (!data || !data.Attributes) {
        return done(null, originalDiscussion);
      }
      return done(null, new Discussion(data.Attributes));
    });
  });
}