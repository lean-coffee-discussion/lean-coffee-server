const AWS = require('aws-sdk');
const AWSConfig = require('../config/dynamodb_local');
const bcrypt = require('bcrypt-nodejs');
const Board = require('./board');

if (process.env.NODE_ENV === 'production') {
  AWS.config.update({ region: process.env.REGION });
} else {
  AWS.config.update(AWSConfig.localConfig);
}

const docClient = new AWS.DynamoDB.DocumentClient();
const tableName = process.env.DB_TABLE_USERS || 'Users';

function User({ email = '', password = '', boards }) {
  this.username = email;
  this.password = password;
  if (boards && boards.values) {
    this.boards = boards.values;
  }
  this.validatePassword = validatePassword;
  this.addBoard = addBoard;
  this.deleteBoard = deleteBoard;
}

function validatePassword(password, done) {
  bcrypt.compare(password, this.password, (err, res) => {
    return done(err, res);
  });
}

function addBoard(title, index, done) {
  if (!title) {
    return done({ statusCode: 400, message: 'No title specified!' });
  }
  Board.insertOne(title, index, (err, board) => {
    if (err) {
      return done(err);
    }
    if (!board || !board.id) {
      return done(null, false);
    }
    const params = {
      TableName: tableName,
      Key: {
        email: this.username
      },
      UpdateExpression: "ADD #attrName :attrValue",
      ExpressionAttributeNames: {
        "#attrName": "boards"
      },
      ExpressionAttributeValues: {
        ":attrValue": docClient.createSet([board.id])
      },
      ReturnValues: "UPDATED_NEW"
    };
    docClient.update(params, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, board);
    });
  });
}

function deleteBoard(id, done) {
  if (!id) {
    return done({ statusCode: 400, message: 'No board ID specified!' });
  }
  if (!this.boards || !this.boards.includes(id)) {
    return done(null, false);
  }
  Board.deleteOne(id, (err, success) => {
    if (err) {
      return done(err);
    }
    if (!success) {
      return done({ statusCode: 400, message: 'Failed to delete board!' });
    }
    const userParams = {
      TableName: tableName,
      Key: {
        email: this.username
      },
      UpdateExpression: "DELETE #attrName :attrValue",
      ExpressionAttributeNames: {
        "#attrName": "boards"
      },
      ExpressionAttributeValues: {
        ":attrValue": docClient.createSet([id])
      }
    }
    docClient.update(userParams, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, true);
    });
  });
}

exports.findOne = (user, done) => {
  if (!user || !user.username) {
    return done({ statusCode: 400, message: 'No username specified!' });
  }
  const params = {
    TableName: tableName,
    Key: {
      email: user.username
    }
  }
  docClient.get(params, function (err, data) {
    if (err) {
      return done(err);
    }
    if (!data || !data.Item) {
      return done(null, false);
    }
    return done(null, new User(data.Item));
  });
}

exports.insertOne = (user, done) => {
  if (!user || !user.username || !user.password) {
    return done({ statusCode: 400, message: 'No credentials specified!' });
  }
  bcrypt.hash(user.password, null, null, (err, passwordHash) => {
    const params = {
      TableName: tableName,
      Item: {
        email: user.username,
        password: passwordHash
      }
    };
    docClient.put(params, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, new User(params.Item));
    });
  });
}