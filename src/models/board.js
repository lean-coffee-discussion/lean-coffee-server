const AWS = require('aws-sdk');
const AWSConfig = require('../config/dynamodb_local');
const cuid = require('cuid');
const Discussion = require('./discussion');

if (process.env.NODE_ENV === 'production') {
  AWS.config.update({ region: process.env.REGION });
} else {
  AWS.config.update(AWSConfig.localConfig);
}

const docClient = new AWS.DynamoDB.DocumentClient();
const tableName = process.env.DB_TABLE_BOARDS || 'Boards';

function Board({ id = '', title = '', index = 0, discussions }) {
  this.id = id;
  this.title = title;
  this.index = index;
  if (discussions && discussions.values) {
    this.discussions = discussions.values;
  }
  this.addDiscussion = addDiscussion;
  this.deleteDiscussion = deleteDiscussion;
  this.updateDiscussion = updateDiscussion;
}

function addDiscussion(title, index, done) {
  if (!title) {
    return done({ statusCode: 400, message: 'No title specified!' });
  }
  Discussion.insertOne(title, index, (err, discussion) => {
    if (err) {
      return done(err);
    }
    if (!discussion || !discussion.id) {
      return done(null, false);
    }
    const params = {
      TableName: tableName,
      Key: {
        id: this.id
      },
      UpdateExpression: "ADD #attrName :attrValue",
      ExpressionAttributeNames: {
        "#attrName": "discussions"
      },
      ExpressionAttributeValues: {
        ":attrValue": docClient.createSet([discussion.id])
      },
      ReturnValues: "UPDATED_NEW"
    };
    docClient.update(params, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, discussion);
    });
  });
}

function updateDiscussion({ id, title, index, topics }, done) {
  if (!id) {
    return done({ statusCode: 400, message: 'No discussion ID specified!' });
  }
  if (!this.discussions || !this.discussions.includes(id)) {
    return done(null, false);
  }
  Discussion.updateOne({ id, title, index, topics }, (err, discussion) => {
    if (err) {
      return done(err);
    }
    return done(null, discussion);
  });
}

function deleteDiscussion(id, done) {
  if (!id) {
    return done({ statusCode: 400, message: 'No discussion ID specified!' });
  }
  if (!this.discussions || !this.discussions.includes(id)) {
    return done(null, false);
  }
  Discussion.deleteOne(id, (err, success) => {
    if (err) {
      return done(err);
    }
    if (!success) {
      return done({ statusCode: 400, message: 'Error deleting discussion!' });
    }
    const userParams = {
      TableName: tableName,
      Key: {
        id: this.id
      },
      UpdateExpression: "DELETE #attrName :attrValue",
      ExpressionAttributeNames: {
        "#attrName": "discussions"
      },
      ExpressionAttributeValues: {
        ":attrValue": docClient.createSet([id])
      }
    }
    docClient.update(userParams, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, true);
    });
  });
}

exports.findOne = (id, done) => {
  if (!id) {
    return done({ statusCode: 400, message: 'No board ID specified!' });
  }
  const params = {
    TableName: tableName,
    Key: {
      id: id
    }
  }
  docClient.get(params, function (err, data) {
    if (err) {
      return done(err);
    }
    if (!data || !data.Item) {
      return done(null, false);
    }
    return done(null, new Board(data.Item));
  });
}

exports.find = (ids, done) => {
  if (!ids || ids.length === 0) {
    return done({ statusCode: 400, message: 'No board IDs specified!' });
  }
  const all = ids.map((id) => { return { id } });
  const params = {
    RequestItems: {
      [tableName]: {
        Keys: all
      }
    }
  }
  docClient.batchGet(params, (err, data) => {
    if (err) {
      return done(err);
    }
    if (!data || !data.Responses || !data.Responses[tableName] || data.Responses[tableName].length === 0) {
      return done(null, false);
    }
    return done(null, data.Responses[tableName].map((board) => new Board(board)));
  });
}

exports.insertOne = (title, index = 0, done) => {
  if (!title) {
    return done({ statusCode: 400, message: 'No title specified!' });
  }
  const params = {
    TableName: tableName,
    Item: {
      id: cuid(),
      title: title,
      index: index
    }
  };
  docClient.put(params, (err, data) => {
    if (err) {
      return done(err);
    }
    return done(null, new Board(params.Item));
  });
}

exports.deleteOne = (id, done) => {
  if (!id) {
    return done({ statusCode: 400, message: 'No board ID specified!' });
  }
  exports.findOne(id, (err, board) => {
    if (err) {
      return done(err);
    }
    if (board.discussions && board.discussions.length > 0) {
      board.discussions.map((discussion) => {
        board.deleteDiscussion(discussion.id, (err, data) => {
          if (err) {
            return done(err);
          }
          if (!data) {
            return done(null, false);
          }
        });
      });
    }
    const params = {
      TableName: tableName,
      Key: {
        id: id
      }
    }
    docClient.delete(params, (err, data) => {
      if (err) {
        return done(err);
      }
      return done(null, true);
    });
  });
}