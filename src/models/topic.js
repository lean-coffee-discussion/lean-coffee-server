const AWS = require('aws-sdk');
const AWSConfig = require('../config/dynamodb_local');
const cuid = require('cuid');

if (process.env.NODE_ENV === 'production') {
  AWS.config.update({ region: process.env.REGION });
} else {
  AWS.config.update(AWSConfig.localConfig);
}

const docClient = new AWS.DynamoDB.DocumentClient();
const tableName = process.env.DB_TABLE_TOPICS || 'Topics';

function Topic({ id = '', title = '', index = 0, votes = 0 }) {
  this.id = id;
  this.title = title;
  this.index = index;
  this.votes = votes;
  this.addVote = addVote;
}

function addVote(done) {
  const newVotes = this.votes + 1;
  const params = {
    TableName: tableName,
    Key: {
      id: this.id
    },
    UpdateExpression: 'SET #attrName = :attrValue',
    ExpressionAttributeNames: {
      "#attrName": "votes"
    },
    ExpressionAttributeValues: {
      ":attrValue": newVotes
    },
    ReturnValues: "ALL_NEW"
  };
  docClient.update(params, (err, data) => {
    if (err) {
      return done(err);
    }
    if (!data || !data.Attributes) {
      return done(null, false);
    }
    return done(null, new Topic(data.Attributes));
  });
}

exports.findOne = (id, done) => {
  if (!id) {
    return done({ statusCode: 400, message: 'No topic ID specified!' });
  }
  const params = {
    TableName: tableName,
    Key: {
      id: id
    }
  }
  docClient.get(params, function (err, data) {
    if (err) {
      return done(err);
    }
    if (!data || !data.Item) {
      return done(null, false);
    }
    return done(null, new Topic(data.Item));
  });
}

exports.find = (ids, done) => {
  if (!ids || ids.length === 0) {
    return done({ statusCode: 400, message: 'No topic IDs specified!' });
  }
  const all = ids.map((id) => { return { id } });
  const params = {
    RequestItems: {
      [tableName]: {
        Keys: all
      }
    }
  }
  docClient.batchGet(params, (err, data) => {
    if (err) {
      return done(err);
    }
    if (!data || !data.Responses || !data.Responses[tableName] || data.Responses[tableName].length === 0) {
      return done(null, false);
    }
    return done(null, data.Responses[tableName].map((topic) => new Topic(topic)));
  });
}

exports.insertOne = (title, index = 0, done) => {
  const params = {
    TableName: tableName,
    Item: {
      id: cuid(),
      title: title,
      index: index
    }
  };
  docClient.put(params, (err, data) => {
    if (err) {
      return done(err);
    }
    return done(null, new Topic(params.Item));
  });
}

exports.deleteOne = (id, done) => {
  const params = {
    TableName: tableName,
    Key: {
      id: id
    }
  }
  docClient.delete(params, (err, data) => {
    if (err) {
      return done(err);
    }
    return done(null, true);
  });
}