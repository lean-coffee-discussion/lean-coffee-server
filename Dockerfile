FROM node:alpine

WORKDIR /usr/app
ADD package.json /usr/app/package.json
RUN npm install
ADD . /usr/app

CMD npm run start