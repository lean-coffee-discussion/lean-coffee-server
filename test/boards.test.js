
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

var boardsToDelete = [];

chai.use(chaiHttp);

describe('Boards Tests', () => {
  afterEach((done) => {
    if (boardsToDelete.length > 0) {
      deleteBoards(boardsToDelete, () => {
        boardsToDelete = [];
        done();
      });
    } else {
      done();
    }
  });

  it('should respond with a list of boards', (done) => {
    chai.request(global.server)
      .get(`${process.env.API_PATH}/boards`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.boards.should.have.length(2);
        done();
      });
  });

  it('should respond with 401 unauthorized for an invalid token', (done) => {
    const invalidToken = 'abc123';
    chai.request(global.server)
      .get(`${process.env.API_PATH}/boards`)
      .set('Authorization', `bearer ${invalidToken}`)
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('should create a new board', (done) => {
    const testTitle = 'A New Board Title';
    const testIndex = 99;
    chai.request(global.server)
      .post(`${process.env.API_PATH}/boards`)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle, index: testIndex })
      .end((err, res) => {
        res.should.have.status(201);
        res.body.id.should.be.a('string');
        res.body.title.should.eql(testTitle);
        res.body.index.should.eql(testIndex);
        boardsToDelete.push(res.body.id);
        done();
      });
  });

  it('should delete a board', (done) => {
    const testTitle = 'A Board To Delete';
    chai.request(global.server)
      .post(`${process.env.API_PATH}/boards`)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle })
      .end((err, res) => {
        const boardId = res.body.id;
        chai.request(global.server)
          .delete(`${process.env.API_PATH}/boards/${boardId}`)
          .set('Authorization', `bearer ${global.authToken}`)
          .end((err, res) => {
            res.should.have.status(204);
            done();
          });
      });
  });

  it('should not delete a board for a different user', (done) => {
    const otherUserBoardId = 'board-3';
    chai.request(global.server)
      .delete(`${process.env.API_PATH}/boards/${otherUserBoardId}`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
  });

  it('should respond with a single board with all board information', (done) => {
    chai.request(global.server)
      .get(`${process.env.API_PATH}/boards`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        const testBoard = res.body.boards[0];
        chai.request(global.server)
          .get(`${process.env.API_PATH}/boards/${testBoard.id}`)
          .set('Authorization', `bearer ${global.authToken}`)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.result.should.eql(testBoard.id);
            res.body.entities.should.not.be.null;
            res.body.entities.boards.should.not.be.null;

            const receivedBoard = res.body.entities.boards[testBoard.id];
            receivedBoard.should.not.be.null;
            receivedBoard.title.should.be.a('string');
            receivedBoard.index.should.be.a('number');
            receivedBoard.discussions.should.have.length(3);

            res.body.entities.discussions.should.not.be.null;
            const receivedDiscussion = res.body.entities.discussions[receivedBoard.discussions[0]];
            receivedDiscussion.should.not.be.null;
            receivedDiscussion.title.should.be.a('string');
            receivedDiscussion.index.should.be.a('number');
            receivedDiscussion.topics.should.have.length(3);

            res.body.entities.topics.should.not.be.null;
            const receivedTopic = res.body.entities.topics[receivedDiscussion.topics[0]];
            receivedTopic.should.not.be.null;
            receivedTopic.title.should.be.a('string');
            receivedTopic.index.should.be.a('number');

            done();
          });
      });
  });

  it('should respond with a 404 not found if a board with an invalid id is requested', (done) => {
    const invalidBoardId = 'AnInvalidBoardId';
    chai.request(global.server)
      .get(`${process.env.API_PATH}/boards/${invalidBoardId}/discussions`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });

  function deleteBoards(boardsArray, done) {
    var remainingCount = boardsArray.length;
    boardsArray.forEach((board) => {
      chai.request(global.server)
        .delete(`${process.env.API_PATH}/boards/${board}`)
        .set('Authorization', `bearer ${global.authToken}`)
        .end((err, res) => {
          remainingCount--;
          if (remainingCount == 0) {
            done();
          }
        });
    });
  }
});