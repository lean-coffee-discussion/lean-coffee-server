
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

let testEndpoint;
let testBoard;
let testDiscussion;
let topicsToDelete = [];

chai.use(chaiHttp);

describe('Topics Tests', () => {
  beforeEach((done) => {
    chai.request(global.server)
      .get(`${process.env.API_PATH}/boards`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        if (res.body.boards.length > 0) {
          testBoard = res.body.boards[0];
        }
        chai.request(global.server)
          .get(`${process.env.API_PATH}/boards/${testBoard.id}/discussions`)
          .set('Authorization', `bearer ${global.authToken}`)
          .end((err, res) => {
            if (res.body.discussions.length > 0) {
              testDiscussion = res.body.discussions[0];
              testEndpoint = `${process.env.API_PATH}/discussions/${testDiscussion.id}/topics`;
            }
            done();
          });
      });
  });

  afterEach((done) => {
    if (topicsToDelete.length > 0) {
      deleteTopics(topicsToDelete, () => {
        topicsToDelete = [];
        done();
      });
    } else {
      done();
    }
  });

  it('should respond with a list of topics', (done) => {
    chai.request(global.server)
      .get(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.topics.should.have.length(2);
        res.body.topics[0].title.should.be.a('string');
        res.body.topics[0].index.should.be.a('number');
        done();
      });
  });

  it('should respond with a 404 not found if an invalid discussion id is requested', (done) => {
    const invalidDiscussionId = 'AnInvalidDiscussionId';
    chai.request(global.server)
      .get(`${process.env.API_PATH}/discussions/${invalidDiscussionId}/topics`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });

  it('should create a new topic', (done) => {
    const testTitle = 'A New Topic Title';
    const testIndex = 99;
    chai.request(global.server)
      .post(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle, index: testIndex })
      .end((err, res) => {
        res.should.have.status(201);
        res.body.id.should.be.a('string');
        res.body.title.should.eql(testTitle);
        res.body.index.should.eql(testIndex);
        topicsToDelete.push(res.body.id);
        done();
      });
  });

  it('should delete a topic', (done) => {
    const testTitle = 'A Topic To Delete';
    chai.request(global.server)
      .post(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle })
      .end((err, res) => {
        const topicId = res.body.id;
        chai.request(global.server)
          .delete(`${testEndpoint}/${topicId}`)
          .set('Authorization', `bearer ${global.authToken}`)
          .end((err, res) => {
            res.should.have.status(204);
            done();
          });
      });
  });

  it('should upvote a topic', (done) => {
    const testTitle = 'A Topic to Vote On';
    const testIndex = 99;
    chai.request(global.server)
      .post(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle, index: testIndex })
      .end((err, res) => {
        const topicId = res.body.id;
        topicsToDelete.push(res.body.id);
        chai.request(global.server)
          .post(`${process.env.API_PATH}/topics/${topicId}/votes`)
          .set('Authorization', `bearer ${global.authToken}`)
          .send({})
          .end((err, res) => {
            res.should.have.status(200);
            res.body.id.should.eql(topicId);
            res.body.votes.should.eql(1);
            done();
          });
      });
  });

  function deleteTopics(topicsArray, done) {
    var remainingCount = topicsArray.length;
    topicsArray.forEach((topic) => {
      chai.request(global.server)
        .delete(`${testEndpoint}/${topic}`)
        .set('Authorization', `bearer ${global.authToken}`)
        .end((err, res) => {
          remainingCount--;
          if (remainingCount == 0) {
            done();
          }
        });
    });
  }
});