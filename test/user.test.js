
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const cuid = require('cuid');

chai.use(chaiHttp);

describe('User Tests', () => {
  it('should respond with login token for a valid user', (done) => {
    chai.request(global.server)
      .get(`${process.env.API_PATH}/login`)
      .auth('testuser@example.com', 'bob')
      .end((err, res) => {
        res.should.have.status(200);
        res.body.token.should.be.a('string');
        done();
      });
  });

  it('should respond with 401 unauthorized for an invalid password', (done) => {
    chai.request(global.server)
      .get(`${process.env.API_PATH}/login`)
      .auth('testuser@example.com', 'IAmAnInvalidPassword')
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('should respond with 401 unauthorized for an invalid user', (done) => {
    chai.request(global.server)
      .get(`${process.env.API_PATH}/login`)
      .auth('invaliduser@example.com', 'IAmAnInvalidPassword')
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('should allow a user to register', (done) => {
    const uniqueUsername = cuid();
    const testEmail = `${uniqueUsername}@example.com`;
    const testPassword = 'AVerySecureP@$$W0rd123';
    chai.request(global.server)
      .post(`${process.env.API_PATH}/register`)
      .send({ email: testEmail, password: testPassword, confirm: testPassword })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.token.should.be.a('string');
        done();
      });
  });

  it('should not allow a user to register required fields are missing', (done) => {
    const testEmail = 'AUserThatShouldNotBeAccepted@Example.com';
    const testPassword = 'AVerySecureP@$$W0rd123';
    chai.request(global.server)
      .post(`${process.env.API_PATH}/register`)
      .send({ email: testEmail, password: testPassword })
      .end((err, res) => {
        res.should.have.status(400);
        res.body.message.should.eql('User email, password and password confirmation are required!');
        done();
      });
  });

  it('should not allow a user to register if the user already exists', (done) => {
    const testEmail = 'TestUser@Example.com';
    const testPassword = 'AVerySecureP@$$W0rd123';
    chai.request(global.server)
      .post(`${process.env.API_PATH}/register`)
      .send({ email: testEmail, password: testPassword, confirm: testPassword })
      .end((err, res) => {
        res.should.have.status(409);
        res.body.message.should.eql('User already exists!');
        done();
      });
  });

  it('should not allow a user to register if the passwords do not match', (done) => {
    const testEmail = 'AUserThatShouldNotBeAccepted@Example.com';
    const testPassword = 'AVerySecureP@$$W0rd123';
    chai.request(global.server)
      .post(`${process.env.API_PATH}/register`)
      .send({ email: testEmail, password: testPassword, confirm: 'DoesNotMatch' })
      .end((err, res) => {
        res.should.have.status(400);
        res.body.message.should.eql('Password does not match!');
        done();
      });
  });

  it('should not allow a user to register if the passwords does not meet minimum complexity', (done) => {
    const testEmail = 'AUserThatShouldNotBeAccepted@Example.com';
    const testPassword = 'MyPa$$Word';
    chai.request(global.server)
      .post(`${process.env.API_PATH}/register`)
      .send({ email: testEmail, password: testPassword, confirm: testPassword })
      .end((err, res) => {
        res.should.have.status(400);
        res.body.message.should.eql('The password must contain at least one number.');
        done();
      });
  });
});