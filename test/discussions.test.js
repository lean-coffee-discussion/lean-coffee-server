
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

let testEndpoint;
let testBoard;
let discussionsToDelete = [];

chai.use(chaiHttp);

describe('Discussions Tests', () => {
  beforeEach((done) => {
    chai.request(global.server)
      .get(`${process.env.API_PATH}/boards`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        if (res.body.boards.length > 0) {
          testBoard = res.body.boards[0];
          testEndpoint = `${process.env.API_PATH}/boards/${testBoard.id}/discussions`;
        }
        done();
      });
  });

  afterEach((done) => {
    if (discussionsToDelete.length > 0) {
      deleteDiscussions(discussionsToDelete, () => {
        discussionsToDelete = [];
        done();
      });
    } else {
      done();
    }
  });

  it('should respond with a list of discussions', (done) => {
    chai.request(global.server)
      .get(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.discussions.should.have.length(3);
        done();
      });
  });

  it('should respond with a 404 not found if a board with an invalid id is requested', (done) => {
    const invalidBoardId = 'AnInvalidBoardId';
    chai.request(global.server)
      .get(`${process.env.API_PATH}/boards/${invalidBoardId}/discussions`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });

  it('should create a new discussion', (done) => {
    const testTitle = 'A New Discussion Title';
    const testIndex = 99;
    chai.request(global.server)
      .post(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle, index: testIndex })
      .end((err, res) => {
        res.should.have.status(201);
        res.body.id.should.be.a('string');
        res.body.title.should.eql(testTitle);
        res.body.index.should.eql(testIndex);
        discussionsToDelete.push(res.body.id);
        done();
      });
  });

  it('should not create a new discussion for another users board', (done) => {
    const otherUserBoardId = 'board-3';
    const testTitle = 'A New Discussion Title';
    chai.request(global.server)
      .post(`${process.env.API_PATH}/boards/${otherUserBoardId}/discussions`)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle })
      .end((err, res) => {
        res.should.have.status(401);
        done();
      });
  });

  it('should delete a discussion', (done) => {
    const testTitle = 'A Discussion To Delete';
    chai.request(global.server)
      .post(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle })
      .end((err, res) => {
        const discussionId = res.body.id;
        chai.request(global.server)
          .delete(`${testEndpoint}/${discussionId}`)
          .set('Authorization', `bearer ${global.authToken}`)
          .end((err, res) => {
            res.should.have.status(204);
            done();
          });
      });
  });

  it('should not delete a discussion for a different user', (done) => {
    const otherUserDiscussionId = 'discussion-7';
    chai.request(global.server)
      .delete(`${testEndpoint}/${otherUserDiscussionId}`)
      .set('Authorization', `bearer ${global.authToken}`)
      .end((err, res) => {
        res.should.have.status(400);
        done();
      });
  });

  it('should update an existing discussion', (done) => {
    const testTitle = 'A New Discussion Title';
    const updatedTitle = 'An Updated Discussion Title';
    const updatedIndex = 99;
    chai.request(global.server)
      .post(testEndpoint)
      .set('Authorization', `bearer ${global.authToken}`)
      .send({ title: testTitle })
      .end((err, res) => {
        const discussionToUpdate = res.body.id;
        discussionsToDelete.push(discussionToUpdate);
        const expectedDiscussion = { id: discussionToUpdate, title: updatedTitle, index: updatedIndex };
        chai.request(global.server)
          .put(`${testEndpoint}/${discussionToUpdate}`)
          .set('Authorization', `bearer ${global.authToken}`)
          .send({ title: updatedTitle, index: updatedIndex })
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.eql(expectedDiscussion);
            done();
          })
      });
  });

  function deleteDiscussions(discussionsArray, done) {
    var remainingCount = discussionsArray.length;
    discussionsArray.forEach((board) => {
      chai.request(global.server)
        .delete(`${testEndpoint}/${board}`)
        .set('Authorization', `bearer ${global.authToken}`)
        .end((err, res) => {
          remainingCount--;
          if (remainingCount == 0) {
            done();
          }
        });
    });
  }
});