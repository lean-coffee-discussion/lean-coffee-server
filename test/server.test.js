process.env.NODE_ENV = 'test';
process.env.JWT_SECRET = 'TestJWTSecret';
process.env.AWS_ACCESS_KEY_ID = 'ATestAPIKey';
process.env.AWS_SECRET_ACCESS_KEY = 'ATestSecretAPIKey';
process.env.HOST_PORT = 8081
process.env.DB_ENDPOINT = process.env.DB_ENDPOINT || 'http://localhost:8001';
process.env.API_PATH = '/api/v1'

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);

before(async () => {
  global.server = require('../src/server.local');
  const res = await chai.request(global.server)
    .get(`${process.env.API_PATH}/login`)
    .auth('testuser@example.com', 'bob');
  global.authToken = res.body.token;
});

after(async () => {
  require('../src/server.local').stop();
});

describe('Server Tests', () => {
  it('should respond with status ok', (done) => {
    const expectedJson = {
      status: 'ok'
    }
    chai.request(global.server)
      .get(`${process.env.API_PATH}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.eql(expectedJson);
        done();
      });
  });
});