# Lean Coffee Server

## Running Tests

The integration tests requires a specific DynamoDB database snapshot to be running locally on port 8001. This can be set up by running a test Docker container.

```
docker pull registry.gitlab.com/garmstro/lean-coffee-database/db-test-suite:test
docker run -d -p 8001:8000 --name test-dynamodb registry.gitlab.com/garmstro/lean-coffee-database/db-test-suite:test
```

After the test database is running the tests can be run using the command:

```
npm run test
```

Alternatively, the tests can be run using Docker compose:

```
docker-compose -f docker-compose.test.yml build
docker-compose -f docker-compose.test.yml run test
```

## Start the Server

```
JWT_SECRET="<YOUR_SECURE_SECRET_KEY>" DB_ENDPOINT="<YOUR_DYNAMO_DB_ENDPOINT>" npm start
```

The DynamoDB Endpoint can be a local DynamoDB, or an AWS hosted version. If it is an AWS hosted database, AWS credentials that have access to the database must be set in the environment. By default the endpoint will be set to localhost:8000 with dummy AWS credentials. Instructions for setting up a local DynamoDB are [here](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/DynamoDBLocal.html).

The server can also be run using Docker compose:

```
docker-compose build
docker-compose up -d
```

This will pull a DynamoDB snapshot docker container that contains the necessary tables for the server to run. This is helpful for developing locally.